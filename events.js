function submitButton(){
	alert('Form submitted!');
}

function changeText(text){

	text.innerHTML = '<div class="d-flex w-100 justify-content-between">'+
      '<h5 class="mb-1">First Event</h5>'+
     ' <small>Now</small></div>'+
    '<p class="mb-1">Text Changed on Hover</p>'+
    '<small>Mouseover Event</small>';
}


var secondPost = document.getElementById('secondPost').addEventListener('dblclick', changeTextOnDblClick);

function changeTextOnDblClick(){
	var second = document.getElementById('secondText');
	second.textContent = 'Mouse Double-Clicked on Second Post.'
}

var thirdPost = document.getElementById('thirdPost');
thirdPost.addEventListener('mouseenter', changeOnMouseEnter);
thirdPost.addEventListener('mouseleave', changeOnMouseLeave);

function changeOnMouseEnter(){
	let thirdText = document.getElementById('thirdText');
	thirdText.innerHTML = 'Mouse Entered in Third Post!';
	thirdPost.style.color = 'Crimson';
	thirdPost.style.background = 'lightblue';
}

function changeOnMouseLeave(){
	let thirdText = document.getElementById('thirdText');
	thirdText.innerHTML = 'Mouse Left the Third Post!';
	thirdPost.style.color = 'blue';
	thirdPost.style.background = 'pink';
}


var inputForm = document.getElementById('submitBtn').addEventListener('mousedown', hideForm);

function hideForm(){
	var form = document.getElementById('inputForm');
	form.style.display = 'none';
}

var home = document.getElementById('showForm').addEventListener('mouseup',showForm);

function showForm(){
	var form = document.getElementById('inputForm');
	form.style.display = 'block';
}

//keyup és keypress ugyanigy
var search = document.getElementById('search').addEventListener('keypress', printToConsole);

function printToConsole(text){
	console.log(text.target.value);
}